  movies = {
  }
  
x=1
until x==2
  
puts "What would you like to do? "
puts "type 'add' to add a movie"
puts "type 'update' to update a rating"
puts "type 'display' to see the movies already added and their rating"
puts "type 'delete' to delete a movie"
puts "type 'exit' to quit the program"

choice = gets.chomp

case choice

when "add"
  puts ""
  puts "What movie would you like to add? "
  title = gets.chomp
  if movies[title.to_sym] != nil
    puts ""
    puts "#{title} already exists in the db."
  else
    puts ""
    puts "What rating does the movie have? "
    rating = gets.chomp
    movies[title.to_sym] = rating.to_i
  end
    puts "Do you want to exit ?"
    puts ""
    puts "'y' or 'n'"
    exit = gets.chomp
    case exit
      when "y"
      puts ""
      puts "Successfully exited."
      x = 2
      else
      puts "Okay ! :)"
    end
    
when "update"
  puts ""
  puts "What movie would you like to update? "
  title = gets.chomp
  if movies[title.to_sym] == nil
    puts ""
    puts "'#{title}' doesn't exists"
  else
    puts ""
    puts "What rating does the movie have? "
    rating = gets.chomp
    movies[title.to_sym] = rating.to_i
  end
    puts "Do you want to exit ?"
    puts ""
    puts "'y' or 'n'"
    exit = gets.chomp
    case exit
      when "y"
      puts ""
      puts "Successfully exited."
      x = 2
      else
      puts "Okay ! :)"
    end
    
when "display"
    puts ""
  movies.each do |movie, rating|
    puts "#{movie}: #{rating}"
  end
    puts "Do you want to exit ?"
    puts ""
    puts "'y' or 'n'"
    exit = gets.chomp
    case exit
      when "y"
      puts ""
      puts "Successfully exited."
      x = 2
      else
      puts "Okay ! :)"
    end
    
when "delete"
  puts ""
  puts "What movie would you like to delete? "
  title = gets.chomp
  if movies[title.to_sym] == nil
    puts ""
    puts "No movie named '#{title}'"
  else 
    movies.delete(title)
    puts ""
    puts "successfully deleted"
  end
    puts "Do you want to exit ?"
    puts ""
    puts "'y' or 'n'"
    exit = gets.chomp
    case exit
      when "y"
      puts ""
      puts "Successfully exited."
      x = 2
      else
      puts "Okay ! :)"
    end
    
when "exit"
    puts ""
    puts "Successfully exited."
  x = 2
else
    puts ""
  puts "Error ! Did u type a valid command?"
end

puts ""

end
